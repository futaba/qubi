import * as React from 'react';
import { render } from 'react-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faReply, faSync, faStar } from '@fortawesome/free-solid-svg-icons';
import {
  Container,
  Columns,
  Navbar,
  Card,
  Media,
  Heading,
  Content,
  Box,
  Image,
  Icon,
  Section,
  Label,
  Control,
  Button
} from 'react-bulma-components/full';
import placeholder from './placeholder.png';

// 助けてくれ　人類

render(
  <div>
    <Container>
      <Navbar className="is-center">
        <Navbar.Brand>
          <Navbar.Item renderAs="a" href="#">
            Qubi
          </Navbar.Item>
        </Navbar.Brand>
        <Navbar.Menu>
          <Navbar.Container>
            <Navbar.Item href="#">Menu</Navbar.Item>
          </Navbar.Container>
        </Navbar.Menu>
        <Navbar.Menu position="end">
          <Navbar.Container position="end">
            <Navbar.Item>
              <Image renderAs="p" size={24} src={placeholder} />
              @test
            </Navbar.Item>
            <Navbar.Item>
              <div className="buttons">
                <Button className="is-primary">Login</Button>
                <Button className="is-default">Logout</Button>
              </div>
            </Navbar.Item>
          </Navbar.Container>
        </Navbar.Menu>
      </Navbar>
    </Container>
    <Container className="container-pad">
      <Columns>
        <Columns.Column size="one-third">
          <Card>
            <Card.Content>
              <Media>
                <Media.Item renderAs="figure" position="left">
                  <Image renderAs="p" size={64} src={placeholder} />
                </Media.Item>
                <Media.Item>
                  <Heading size={5}>Test</Heading>
                  <Heading subtitle size={6}>
                    @test
                  </Heading>
                </Media.Item>
              </Media>
              <Content>
                <div className="field">
                  <div className="control is-expanded">
                    <textarea className="textarea" placeholder="What's up?" />
                  </div>
                  <div className="control is-expanded">
                    <Button className="button is-info" type="submit">
                      Send
                    </Button>
                  </div>
                </div>
              </Content>
            </Card.Content>
            <Card.Footer>
              <Card.Footer.Item renderAs="a" href="#Yes">
                1
              </Card.Footer.Item>
              <Card.Footer.Item renderAs="a" href="#No">
                2
              </Card.Footer.Item>
              <Card.Footer.Item renderAs="a" href="#Maybe">
                3
              </Card.Footer.Item>
            </Card.Footer>
          </Card>
        </Columns.Column>

        <Columns.Column size="auto">
          <Box>
            {[1, 2, 3, 4, 5].map(() => {
              return (
                <Media>
                  <Media.Item renderAs="figure" position="left">
                    <Image renderAs="p" size={64} src={placeholder} />
                  </Media.Item>
                  <Media.Item>
                    <Content>
                      <p>
                        <p>
                          <strong>Test</strong> <small>@test</small>{' '}
                          <small className="is-pulled-right">31m</small>
                        </p>

                        <p>
                          何で負けたか、明日まで考えといてください。そしたら何かが見えてくるはずです。
                          ほな、いただきます。
                        </p>
                        <div className="field has-addons">
                          <p className="control">
                            <a className="button">
                              <FontAwesomeIcon icon={faReply} />
                              &nbsp;
                              <span>0</span>
                            </a>
                          </p>
                          <p className="control">
                            <a className="button">
                              <FontAwesomeIcon icon={faSync} />
                              &nbsp;
                              <span>0</span>
                            </a>
                          </p>
                          <p className="control">
                            <a className="button">
                              <FontAwesomeIcon icon={faStar} />
                              &nbsp;
                              <span>0</span>
                            </a>
                          </p>
                        </div>
                      </p>
                    </Content>
                  </Media.Item>
                </Media>
              );
            })}
          </Box>
        </Columns.Column>
      </Columns>
    </Container>
  </div>,
  document.getElementById('app')
);
